import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Tools } from './tools.service';
import { DicoService } from '../language/dico.service';
import { ListService } from '../providers/list.service';
import { EventsService, Event } from '../events/events-service/events.service';
import { VoteService, Poll } from '../vote/vote-service/vote.service';
import { Observable, pipe, of, combineLatest } from 'rxjs';
import { map, shareReplay, catchError } from 'rxjs/operators';

const index = {
	"events": -3,
	"calendar": -2,
	"actus": 2,
	"nsigma": 2,
	"jobads": 2,
	"votes": 4,
	"cafet": 5,
	"infos": 6
};

export class AppModule {
	name: string;
	displayName: string;
	fullName: string;
	description: string;
	disabled: boolean;
	content: boolean;
	order: number;

	constructor(
		name: string, displayName: string, fullName: string, description: string,
		disabled: boolean, content: boolean
	) {
		this.name = name;
		this.displayName = displayName;
		this.fullName = fullName;
		this.description = description;
		this.disabled = disabled;
		this.content = content;
	}
}

@Injectable()
export class AppModulesService {

	private modules: AppModule[] = [];
	private _modulesObs: Observable<AppModule[]>;

	constructor(
		private db: AngularFireDatabase,
		private d: DicoService,
		private list: ListService,
		private events: EventsService,
		private votes: VoteService
	) {
		for (let mod of Object.getOwnPropertyNames(index)) {
			this.modules.push(
				new AppModule(
					mod,
					this.d.l[mod + 'ModuleDisplayName'],
					this.d.l[mod + 'ModuleFullName'],
					this.d.l[mod + 'ModuleDescription'],
					false,
					["events"].includes(mod) ? true : false,
				)
			);
		}
	}

	getAppModules(): Observable<AppModule[]> {
		if (!this._modulesObs) {
			this._modulesObs = combineLatest(
				of(this.modules),
				this.votes.getPollsIDidntAnswer().pipe(
					catchError(error => of([]))
				)
			).pipe(
				map<[AppModule[], Poll[]], AppModule[]>(
					([modules, polls]: [AppModule[], Poll[]]) => modules.map(mod => {
						mod.order = index[mod.name];

						if (mod.name === "votes" && polls.length > 0) {
							mod.order = -mod.order;
						}
						return mod;
					})
				),
				map(modules => modules.filter(
					mod => {
						return !mod.disabled;
					}
				)),
				map(modules => modules.sort(
					(mod1, mod2) => mod1.order - mod2.order
				)),
				shareReplay(1)
			);
		}
		return this._modulesObs;
	}
}
