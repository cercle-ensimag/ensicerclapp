import { Component, OnInit } from '@angular/core';
import { DicoService } from '../../language/dico.service'


@Component({
	selector: 'app-faq',
	templateUrl: './faq.component.html',
	styleUrls: ['../info.component.css']
})
export class FAQComponent implements OnInit {

	constructor(
		public d: DicoService
	) { }

	ngOnInit() { }
}
