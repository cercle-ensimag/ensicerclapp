import { Component, OnInit } from '@angular/core';

import { CafetService } from '../../cafet-service/cafet.service';
import { DicoService } from '../../../language/dico.service';

import { Observable } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-trezo',
	templateUrl: './trezo.component.html',
	styleUrls: ['./trezo.component.css']
})
export class TrezoComponent implements OnInit {

	constructor(
		public cafet: CafetService,
		public d: DicoService,
	) { }

	ngOnInit() { }

	// Credits
	get totalOfPositive(): Observable<string> {
		return this.cafet.getUsers().pipe(
			map(users => users.filter(user => user.credit >= 0).reduce((sum, user) => sum + user.credit, 0).toFixed(2))
		);
	}

	get totalOfNegative(): Observable<string> {
		return this.cafet.getUsers().pipe(
			map(users => users.filter(user => user.credit < 0).reduce((sum, user) => sum + user.credit, 0).toFixed(2))
		);
	}

	get totalOnAccounts(): Observable<string> {
		return this.cafet.getUsers().pipe(
			map(users => users.reduce((sum, user) => sum + user.credit, 0).toFixed(2))
		);
	}

	get nbOfPosAccounts(): Observable<number> {
		return this.cafet.getUsers().pipe(
			map(users => users.filter(user => user.credit >= 0).length)
		);
	}

	get nbOfNegAccounts(): Observable<number> {
		return this.cafet.getUsers().pipe(
			map(users => users.filter(user => user.credit < 0).length)
		);
	}

	get nbOfAllAccounts(): Observable<number> {
		return this.cafet.getUsers().pipe(
			map(users => users.length)
		);
	}
}
