
import { map, first, shareReplay } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { CafetService, CafetUser, DayTransaction } from '../../cafet-service/cafet.service';
import { DicoService } from '../../../language/dico.service';
import { Observable } from 'rxjs';


@Component({
	selector: 'app-cafet-admin-accounts',
	templateUrl: './cafet-admin-accounts.component.html',
	styleUrls: ['./cafet-admin-accounts.component.css']
})
export class CafetAdminAccountsComponent implements OnInit {
	public editing: boolean = false;
	public reviewing: boolean = false;
	public pdf = null;
	private _dayTransactions: Observable<{user: CafetUser, transactions: DayTransaction}[]>;

	constructor(
		private snackBar: MatSnackBar,
		private dialog: MatDialog,
		public cafet: CafetService,
		public d: DicoService
	) { }

	ngOnInit() { }

	getDayTransactions() {
		if (!this._dayTransactions){
			this._dayTransactions = this.cafet.getDayTransactions().pipe(
				map(data => {
					const dayTransactions = [];
					Object.getOwnPropertyNames(data).forEach((emailId) => {
						const transactions = [];
						Object.getOwnPropertyNames(data[emailId]).forEach(transId => {
							transactions.push({
								id: transId,
								value: data[emailId][transId].value,
								date: data[emailId][transId].date,
								resp: data[emailId][transId].resp
							});
						});
						dayTransactions.push({
							user: {
								emailId: emailId,
								activated: null,
								credit: null,
								creationDate: null,
								lastTransactionDate: null,
								profile: null
							},
							transactions: transactions
						})
					});
					return dayTransactions;
				})
			).pipe(
				shareReplay(1)
			);
		}
		return this._dayTransactions;
	}

	validateDayTransactions() {
		this.cafet.validateDayTransactions().then(
			() => {},
			(error) => {
				this.snackBar.open(error, this.d.l.ok, {duration: 2000});
			}
		);
	}

	deleteDayTransaction(emailId: string, transId: string) {
		this.cafet.deleteDayTransaction(emailId, transId);
	}

	printAccountsPdf() {
		this.cafet.getUsers().pipe(
			first()
		).subscribe(users => {
			this.pdf = this.cafet.printAccountsPdf(users);
		});
	}

	saveAccountsPdf() {
		this.cafet.getUsers().pipe(
			first()
		).subscribe(users => {
			this.pdf = this.cafet.saveAccountsPdf(users);
		});
	}

}
