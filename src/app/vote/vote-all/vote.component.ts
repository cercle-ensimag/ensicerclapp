import { Component, OnInit } from '@angular/core';
import { VoteService } from '../vote-service/vote.service';
import { ListService } from '../../providers/list.service';
import { DicoService } from '../../language/dico.service';
import { Location } from '@angular/common';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
	selector: 'app-vote',
	templateUrl: './vote.component.html',
	styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit {
	constructor(
		public vote: VoteService,
		public list: ListService,
		public d: DicoService,
		public location: Location
	) { }

	ngOnInit() {}

	getUserRegistered(): Observable<string> {
		return this.list.isLoggedUserInList().pipe(
			map(is => {
				if (is) {
					return "on";
				} else {
					return "off"
				}
			})
		)
	}

}
