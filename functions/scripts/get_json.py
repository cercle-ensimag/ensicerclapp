"""
This script takes raw emails list and compile them into a JSON associating emailID to email
The first argument is the destination file
The arguments after than are souce files
"""

import sys

if len(sys.argv) < 2:
	print("usage : python3 get_json.py [out filename] [in filename]...")

emails_set = set()

for source in sys.argv[2:]:
	with open(source, "r") as file:
		emails = file.read()

	emails = emails.split("\n")

	for email in emails:
		if email != "":
			emails_set.add(email.lower())

EMAILS_JSON = "{\n"
for email in emails_set:
	EMAILS_JSON += '\t"' + email.split("@")[0].replace(".", "|") + '": "' + email + '",\n'
EMAILS_JSON = EMAILS_JSON[:-2] + "\n}\n"

file = open(sys.argv[1], "w")
file.write(EMAILS_JSON)
file.close()
