"""
Script that generates a file containing emails from a first list
that are already in other lists
"""

import sys

if len(sys.argv) < 3:
	print(
			"usage : python3 get_json.py [out filename] [in new emails filename]\
 [in old emails filename]..."
	)

EMAILS_SET = set()

# list old emails
for source in sys.argv[3:]:
	print(source)
	with open(source, "r") as file:
		emails = file.read()

	emails = emails.split("\n")

	for email in emails:
		if email != "":
			EMAILS_SET.add(email.lower())

NEW_EMAILS_SET = set()

# new emails to campare
with open(sys.argv[2]) as file:
	NEW_EMAILS = file.read()

NEW_EMAILS = NEW_EMAILS.split("\n")
for mail in NEW_EMAILS:
	# only keep left part of the email
	mail = mail.lower().split("@")[0]
	if mail + "@ensimag.fr" in EMAILS_SET:
		NEW_EMAILS_SET.add(mail)
	if mail + "@phelma.grenoble-inp.fr" in EMAILS_SET:
		NEW_EMAILS_SET.add(mail)

with open(sys.argv[1], "w") as file:
	file.write("\n".join(NEW_EMAILS_SET) + "\n")
