"""
Common functions to be used multiple time or in multiple modules
"""
from frtdb_rules_generator.common import do_and, do_or

EMAIL_DOMAIN = "grenoble-inp.org"

def compute_email_id():
	"""
	Returns a string that evalutes to the user email_id
	"""
	return (
			"auth.token.email"
			+ ".replace('@" + EMAIL_DOMAIN + "', '')"
			+ ".replace('.', '|')"
	)

def is_member():
	"""
	Returns a condition that is true if a user can have access to public data
	in the app
	"""
	return "auth !== null && auth.token.email_verified == true"

def has_allowed_email(email):
	"""
	Returns a condition that is true if the given email is authorized to have an
	account
	"""
	return email + ".endsWith('@" + EMAIL_DOMAIN + "')"

def validate_uid(uid):
	"""
	Returns a condition that is true if the given uid is the same as
	the one of user identified
	"""
	return uid + " === auth.uid"

def validate_email_id(email_id):
	"""
	Returns a condition that is true if the given email_id is the same as
	the one of user identified
	"""
	return do_and([
			is_member(),
			email_id + " === " + compute_email_id()
	])

def verify_email_id(email_id):
	"""
	Returns a condition that is true if the given email_id actually belong to
	the user identified
	"""
	return do_and([
			validate_email_id(email_id),
			"root.child('users/'+" + email_id + "+'/'+auth.uid).exists()",
			verify_email_in_list(email_id, "auth.token.email")
	])

def verify_email_in_list(email_id, email):
	"""
	Returns a condition that is true if the given email_id and email are a match
	"""
	return email + " === root.child('list/users/'+{0}).val()".replace("{0}", email_id)

def is_admin():
	"""
	Returns a condition that is true if the user identified is an admin
	"""
	admin_email = "auth.token.email === root.child('admin/private/admins/admin{0}').val()"
	rule = do_or([admin_email.replace("{0}", str(i)) for i in range(1, 4)])
	rule = do_and([rule, is_member()])
	return rule

def is_one_admin(domain):
	"""
	Returns a condition that is true if the authentified user is admin of the given domain
	"""
	path = "'users/'+" + compute_email_id() + "+'/'+auth.uid+'/admin/" + domain + "-admin'"
	one_admin = "root.child(" + path + ").val() == true"
	return one_admin + " && " + is_member()
