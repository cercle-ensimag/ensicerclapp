"""
A module dedicated to list data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, do_or, do_and
from common import is_admin, is_one_admin, has_allowed_email
from vote_rules import is_assessor

class ListRules(RulesPattern):
	"""
	Main node for list rules
	"""
	def __build__(self):
		self.add(CommandRules("deleteNotVerifiedEmails"))
		self.add(CommandRules("getNotVerifiedEmails"))
		self.add(CommandRules("update"))
		self.add(UsersRules("users"))
		self.add(OtherRules())

class CommandRules(RulesPattern):
	"""
	Node list/deleteNotVerifiedEmails
	Node list/getNotVerifiedEmails
	Node list/update
	"""
	def __build__(self):
		self.set_validate("false") # NB: only admin SDK authorized to edit
		self.set_inline(True)

class UsersRules(RulesPattern):
	"""
	Node list/users
	"""
	def __build__(self):
		self.add(UserIdRules("$userId"))

class UserIdRules(RulesPattern):
	"""
	Node list/users/$userId
	"""
	def __build__(self):
		self.set_read(do_or([
				is_admin(),
				is_one_admin("vote"), is_one_admin("cafet"),
				is_one_admin("events"), is_one_admin("actus"),
				is_assessor(),
				"auth.token.email === data.val()"
		]))
		self.set_validate(do_and([
				StringRules.validate_string(200),
				has_allowed_email("newData.val()")
		]))
