"""
A module dedicated to vote data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, BooleanRules, IdRules
from frtdb_rules_generator.common import do_and, do_or, do_not
from common import compute_email_id, is_one_admin, verify_email_id, is_member

def get_assessor_path():
	"""
	Returns the path in the database for assessor data
	"""
	return "'vote/assessors/'+" + compute_email_id()

def is_assessor():
	"""
	Returns a condition that is true if the authentified user is assessor
	"""
	assessor = "root.child(" + get_assessor_path() + ").exists()"
	return assessor + " && " + is_member()

def poll_started(poll_id):
	"""
	Returns a condition that is true if the given poll has started
	"""
	return "root.child('vote/polls/'+" + poll_id + "+'/started').val() == true"


class VoteRules(RulesPattern):
	"""
	Main node for vote rules
	"""
	def __build__(self):
		self.add(AssessorsRules("assessors"))
		self.add(PollsRules("polls"))
		self.add(AssessedVotesRules("votes"))
		self.add(ResultsRules("results"))
		self.add(UsersVotesRules("users"))
		self.add(OtherRules())


# assessor node
class AssessorsRules(RulesPattern):
	"""
	Node vote/assessors
	"""
	def __build__(self):
		self.set_read(do_or([is_one_admin("vote"), is_assessor()]))
		self.set_write(is_one_admin("vote"))
		self.add(AssessorRules("$emailId"))

class AssessorRules(RulesPattern):
	"""
	Node vote/assessors/$emailId
	"""
	def __build__(self):
		self.add(IdRules("emailId", self.check_variable(self.label)))
		self.add(OtherRules())


# polls node
class PollsRules(RulesPattern):
	"""
	Node vote/polls
	"""
	def __build__(self):
		self.set_read(is_member())
		self.set_write(is_one_admin("vote"))
		self.add(PollRules("$poll_id"))

class PollRules(RulesPattern):
	"""
	Node vote/polls/$poll_id
	"""
	def __build__(self):
		children = ["id", "title", "description"]
		self.set_validate("newData.hasChildren(['" + "', '".join(children) + "'])")

		self.add(IdRules(children[0], self.check_variable(self.label)))
		self.add(StringRules(children[1], 30))
		self.add(StringRules(children[2], 500))
		self.add(BooleanRules("started"))
		self.add(ChoicesRules("choices", self.variables))
		self.add(OtherRules())

class ChoicesRules(RulesPattern):
	"""
	Node vote/polls/$poll_id/choices
	"""
	def __build__(self):
		self.add(ChoiceRules("$choice_id", self.variables))

class ChoiceRules(RulesPattern):
	"""
	Node vote/polls/$poll_id/choices/$choice_id
	"""
	def __build__(self):
		children = ["id", "label"]
		self.set_validate("newData.hasChildren(['" + "', '".join(children) + "'])")

		self.add(IdRules(children[0], self.check_variable(self.label)))
		self.add(StringRules(children[1], 30))
		self.add(StringRules("short", 30))
		self.add(StringRules("image", 500))
		self.add(OtherRules())


# assessed votes node
class AssessedVotesRules(RulesPattern):
	"""
	Node vote/votes
	"""
	def __build__(self):
		self.add(PollVotesRules("$poll_id"))

class PollVotesRules(RulesPattern):
	"""
	Node vote/votes/$poll_id
	"""
	def __build__(self):
		self.set_read(do_or([is_assessor(), is_one_admin("vote")]))
		self.set_write(do_and([is_one_admin("vote"), "!newData.exists()"]))
		self.set_validate("root.child('vote/polls/'+" + self.check_variable(self.label) + ").exists()")
		self.add(PollUserVoteRules("$emailId", self.variables))

class PollUserVoteRules(RulesPattern):
	"""
	Node vote/votes/$poll_id/$emailId
	"""
	def __build__(self):
		self.set_write(do_and([
				is_assessor(),
				poll_started(self.check_variable("$poll_id"))
		]))

		children = ["emailId", "voted"]
		self.set_validate("newData.hasChildren(['" + "', '".join(children) + "'])")

		self.add(PollUserVoteEmailIdRules(children[0], self.variables))
		self.add(PollUserVoteAssessedRules(children[1], self.variables))
		self.add(OtherRules())

class PollUserVoteEmailIdRules(RulesPattern):
	"""
	Node vote/votes/$poll_id/$emailId/emailId
	"""
	def __build__(self):
		self.set_write(do_and([
				verify_email_id(self.check_variable("$emailId")),
				poll_started(self.check_variable("$poll_id"))
		]))
		self.set_validate(IdRules.validate_identity(self.check_variable("$emailId")))

class PollUserVoteAssessedRules(RulesPattern):
	"""
	Node vote/votes/$poll_id/$emailId/voted
	"""
	def __build__(self):
		self.set_write(do_and([
				verify_email_id(self.check_variable("$emailId")),
				poll_started(self.check_variable("$poll_id")),
				"(!data.exists() || data.val() != true) && newData.val() == true"
		]))
		self.set_validate(BooleanRules.validate_boolean())


# results node
class ResultsRules(RulesPattern):
	"""
	Node vote/results
	"""
	def __build__(self):
		self.add(PollResultsRules("$poll_id"))

class PollResultsRules(RulesPattern):
	"""
	Node vote/results/$poll_id
	"""
	def __build__(self):
		self.set_write(do_and([
				is_one_admin("vote"),
				"!newData.exists()"
		]))
		self.set_validate("root.child('vote/polls/'+" + self.check_variable(self.label) + ").exists()")

		self.add(BufferRules("buffer", self.variables))
		self.add(BallotBoxRules("ballot_box", self.variables))
		self.add(OtherRules())

class BufferRules(RulesPattern):
	"""
	Node vote/results/$poll_id/buffer
	"""
	def __build__(self):
		self.add(NamedEnveloppesRules("$emailId", self.variables))

class NamedEnveloppesRules(RulesPattern):
	"""
	Node vote/results/$poll_id/buffer/$emailId
	"""
	def __build__(self):
		poll_id = self.check_variable("$poll_id")
		email_id = self.check_variable(self.label)
		self.set_write(do_and([
				verify_email_id(email_id),
				poll_started(poll_id),
				"root.child('vote/users/'+" + poll_id + "+'/'+" + email_id + "+'/voted').val() != true",
				"!data.exists()"
		]))

		choice_path = "vote/polls/'+" + self.check_variable("$poll_id") + "+'/choices/"
		self.set_validate("root.child('" + choice_path + "'+newData.val()).exists()")

class BallotBoxRules(RulesPattern):
	"""
	Node vote/results/$poll_id/ballot_box
	"""
	def __build__(self):
		self.set_read(do_and([
				is_one_admin("vote"),
				do_not(poll_started(self.check_variable("$poll_id")))
		]))
		self.add(BallotByChoicesRules("$choice_id", self.variables))

class BallotByChoicesRules(RulesPattern):
	"""
	Node vote/results/$poll_id/ballot_box/$choice_id
	"""
	def __build__(self):
		poll_id = self.check_variable("$poll_id")
		choice_id = self.check_variable("$choice_id")
		self.set_validate(
				"root.child('vote/polls/'+" + poll_id + "+'/choices/'+" + choice_id + ").exists()"
		)
		self.add(BallotRules("$paper"))

class BallotRules(RulesPattern):
	"""
	Node vote/results/$poll_id/ballot_box/$choice_id/$paper
	"""
	def __build__(self):
		self.set_validate("!data.exists() && newData.val() == true")
		self.set_inline(True)


# users votes node, same data as  assessed votes node, but deffirent point of vue
class UsersVotesRules(RulesPattern):
	"""
	Node vote/users
	"""
	def __build__(self):
		self.add(UserVotesRules("$emailId"))

class UserVotesRules(RulesPattern):
	"""
	Node vote/users/$emailId
	"""
	def __build__(self):
		self.add(UserPollsVotesRules("votes", self.variables))
		self.add(OtherRules())

class UserPollsVotesRules(RulesPattern):
	"""
	Node vote/users/$emailId/votes
	"""
	def __build__(self):
		self.set_read(verify_email_id(self.check_variable("$emailId")))
		self.add(UserPollVotesRules("$poll_id", self.variables))

class UserPollVotesRules(RulesPattern):
	"""
	Node vote/users/$emailId/votes/$poll_id
	"""
	def __build__(self):
		self.set_write(do_and([
				do_or([verify_email_id(self.check_variable("$emailId")), is_assessor()]),
				poll_started(self.check_variable("$poll_id"))
		]))
		self.set_validate(do_and([
				"root.child('vote/polls/'+" + self.check_variable("$poll_id") + ").exists()",
				"newData.isBoolean() && newData.val() == true"
		]))
