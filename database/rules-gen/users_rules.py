"""
A module dedicated to users data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, BooleanRules
from common import is_admin, validate_uid, verify_email_in_list

class UsersRules(RulesPattern):
	"""
	Main node for users rules
	"""
	def __build__(self):
		self.read = is_admin()
		self.add(AuthUserRules("$emailId"))


class AuthUserRules(RulesPattern):
	"""
	Node users/$emailId
	"""
	def __build__(self):
		self.add(UidRules("uid"))
		self.add(UserRules("$user", self.variables))


class UidRules(RulesPattern):
	"""
	Node users/$emailId/uid
	"""
	def __build__(self):
		self.set_validate("false")
		self.set_inline(True)


class UserRules(RulesPattern):
	"""
	Node users/$emailId/$user
	"""
	def __build__(self):
		self.set_read(validate_uid(self.check_variable("$user")))

		self.add(AdminPartRules("admin", self.variables))
		self.add(AccountPartRules("account", self.variables))
		self.add(OtherRules())

# Admin node
class AdminPartRules(RulesPattern):
	"""
	Node users/$emailId/$user/admin
	"""
	def __build__(self):
		self.write = is_admin()

		self.add(EmailRules("email", self.variables))
		self.add(BooleanRules("cafet-admin"))
		self.add(BooleanRules("events-admin"))
		self.add(BooleanRules("actus-admin"))
		self.add(BooleanRules("vote-admin"))
		self.add(BooleanRules("nsigma-admin"))
		self.add(BooleanRules("jobads-admin"))
		self.add(OtherRules())

class EmailRules(RulesPattern):
	"""
	Node users/$emailId/$user/admin/email
	"""
	def __build__(self):
		self.set_validate(verify_email_in_list(self.check_variable("$emailId"), "newData.val()"))
		self.set_inline(True)


# Account node
class AccountPartRules(RulesPattern):
	"""
	Node users/$emailId/$user/account
	"""
	def __build__(self):
		self.set_write(validate_uid(self.check_variable("$user")))

		self.add(NameRules("name"))
		self.add(StringRules("photoURL", 500))
		self.add(OtherRules())

class NameRules(RulesPattern):
	"""
	Node users/$emailId/$user/account/name
	"""
	def __build__(self):
		child_label_1 = "firstName"
		child_label_2 = "lastName"
		self.set_validate("newData.hasChildren(['" + child_label_1 + "', '" + child_label_2 + "'])")

		self.add(StringRules(child_label_1, 30))
		self.add(StringRules(child_label_2, 30))
		self.add(StringRules("login", 30))

		self.add(OtherRules())
