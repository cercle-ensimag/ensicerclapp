"""
A module dedicated to admin data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, do_and
from common import is_admin, is_member, has_allowed_email

class AdminRules(RulesPattern):
	"""
	Main node for admin rules
	"""
	def __build__(self):
		self.add(PublicRules("public"))
		self.add(PrivateRules("private"))
		self.add(OtherRules())


# public node
class PublicRules(RulesPattern):
	"""
	Node admin/public
	"""
	def __build__(self):
		self.set_read(is_member())
		self.add(OtherRules())


# private node
class PrivateRules(RulesPattern):
	"""
	Node admin/private
	"""
	def __build__(self):
		self.set_read(is_admin())

		self.add(AdminsRules("admins"))
		self.add(OtherRules())

class AdminsRules(RulesPattern):
	"""
	Node admin/private/admins
	"""
	def __build__(self):
		self.add(AdminEmailsRules("$email"))

class AdminEmailsRules(RulesPattern):
	"""
	Node admin/private/admins/$email
	"""
	def __build__(self):
		self.set_validate(do_and([
				StringRules.validate_string(200),
				has_allowed_email("newData.val()")
		]))
		self.set_inline(True)
