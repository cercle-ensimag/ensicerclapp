"""
A module dedicated to Nsigma data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, BooleanRules, StringRules
from frtdb_rules_generator.common import BornedNumberRules, NumberRules, IdRules
from common import is_member, is_one_admin

class NsigmaRules(RulesPattern):
	"""
	Main node for nsigma rules
	"""
	def __build__(self):
		self.add(NsigmaJobAdsListRules("jobads"))
		self.add(OtherRules())

# nsigma jobads list node
class NsigmaJobAdsListRules(RulesPattern):
	"""
	Node nsigma/jobads
	"""
	def __build__(self):
		self.set_read(is_member())
		self.set_write(is_one_admin("nsigma"))
		self.index_on = ["start"]
		self.add(NsigmaJobAdRules("$nsigmaJobAdId"))

# nsigma jobad node
class NsigmaJobAdRules(RulesPattern):
	"""
	Node nsigma/jobads/$nsigmaJobAdId
	"""
	def __build__(self):
		self.set_write(is_one_admin("nsigma"))

		self.add(IdRules("id", self.check_variable(self.label)))
		self.add(StringRules("title", 50))
		self.add(StringRules("description", 5000))
		self.add(BornedNumberRules("type", 0, 6))
		self.add(NumberRules("start"))
		self.add(NumberRules("end"))
		self.add(StringRules("technologies", 100))
		self.add(StringRules("difficulty", 100))
		self.add(BornedNumberRules("remuneration", 0, 500000))
		self.add(StringRules("form", 100))
		self.add(BooleanRules("done"))
		self.add(OtherRules())
