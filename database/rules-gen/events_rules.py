"""
A module dedicated to events data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, NumberRules
from frtdb_rules_generator.common import IdRules, do_and, do_or
from common import compute_email_id, is_member, is_one_admin

def get_comresp_path():
	"""
	Returns the path in the database for comResp data
	"""
	return "'events/com-resps/resps/'+" + compute_email_id()

def is_comresp():
	"""
	Returns a condition that is true if the authentified user is comResp
	"""
	comresp = "root.child(" + get_comresp_path() + ").exists()"
	return comresp + " && " + is_member()

def comresp_group_exists(group_id):
	"""
	Returns a condition that is true if the given group_id (newData.val())
	matches a group_id of the authentified user
	"""
	return do_or([
			do_and([
					"root.child(" + get_comresp_path() + "+'/groupId" + str(i) + "').exists()",
					"root.child(" + get_comresp_path() + "+'/groupId" + str(i) + "').val() !== null",
					"root.child(" + get_comresp_path() + "+'/groupId" + str(i) + "').val() !== ''",
					"root.child(" + get_comresp_path() + "+'/groupId" + str(i) + "').val() === " + group_id
			]) for i in range(1, 3)
	])

def comresp_group_the_same(snap):
	"""
	Returns a condition that is true if on of the comResp group_id matches
	one of the event group_id
	"""
	return do_or([
			comresp_group_exists(snap + ".child('groupId" + str(i) + "').val()") for i in range(1, 4)
	])

def event_group_exists(group_id):
	"""
	Returns a condition that is true if the given group_id is defined
	"""
	return "root.child('events/com-resps/groups/'+" + group_id + ").exists()"


class EventsRules(RulesPattern):
	"""
	Main node for events rules
	"""
	def __build__(self):
		self.add(ComRespsRules("com-resps"))
		self.add(EventsListRules("events"))
		self.add(OtherRules())


# common
class GroupIdRules(RulesPattern):
	"""
	Node events/com-resps/resps/$emailId/groupId1
	Node events/com-resps/resps/$emailId/groupId2
	Node events/events/$eventId/groupId1
	Node events/events/$eventId/groupId2
	Node events/events/$eventId/groupId3
	"""
	def __build__(self):
		self.set_validate(do_and([
				StringRules.validate_string(30),
				event_group_exists("newData.val()")
		]))
		self.set_inline(True)


# com resps node
class ComRespsRules(RulesPattern):
	"""
	Node events/com-resps
	"""
	def __build__(self):
		self.set_read(do_or([is_comresp(), is_one_admin("events")]))
		self.set_write(is_one_admin("events"))

		self.add(GroupsRules("groups"))
		self.add(UsersRules("resps"))
		self.add(OtherRules())

class GroupsRules(RulesPattern):
	"""
	Node events/com-resps/groups
	"""
	def __build__(self):
		self.set_read(is_member())
		self.add(GroupRules("$groupId"))

class GroupRules(RulesPattern):
	"""
	Node events/com-resps/groups/$groupId
	"""
	def __build__(self):
		self.add(StringRules("groupId", 30))
		self.add(StringRules("displayName", 30))
		self.add(OtherRules())

class UsersRules(RulesPattern):
	"""
	Node events/com-resps/resps
	"""
	def __build__(self):
		self.add(UserRules("$emailId"))

class UserRules(RulesPattern):
	"""
	Node events/com-resps/resps/$emailId
	"""
	def __build__(self):
		self.add(IdRules("emailId", self.check_variable(self.label)))
		self.add(GroupIdRules("groupId1"))
		self.add(GroupIdRules("groupId2"))
		self.add(OtherRules())


# events node
class EventsListRules(RulesPattern):
	"""
	Node events/events
	"""
	def __build__(self):
		self.set_read(is_member())
		self.index_on = ["start", "end"]
		self.add(EventRules("$eventId"))

class EventRules(RulesPattern):
	"""
	Node events/events/$eventId
	"""
	def __build__(self):
		self.set_write(do_or([
				is_one_admin("events"),
				do_and([
						is_comresp(),
						do_or([
								comresp_group_the_same("newData"),
								do_and([
										"!newData.exists()",
										comresp_group_the_same("data")
								])
						])
				])
		]))

		self.add(IdRules("id", self.check_variable("$eventId")))
		self.add(StringRules("title", 50))
		self.add(StringRules("description", 2000))
		self.add(StringRules("image", 500))
		self.add(NumberRules("start"))
		self.add(NumberRules("end"))
		self.add(StringRules("location", 300))
		self.add(StringRules("price", 50))
		self.add(GroupIdRules("groupId1"))
		self.add(GroupIdRules("groupId2"))
		self.add(GroupIdRules("groupId3"))
		self.add(OtherRules())
