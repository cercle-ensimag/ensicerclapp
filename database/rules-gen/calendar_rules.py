"""
A module dedicated to calendar data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import StringRules, NumberRules, BornedNumberRules
from frtdb_rules_generator.common import BooleanRules, OtherRules, IdRules
from common import validate_uid

class CalendarRules(RulesPattern):
	"""
	Main node for calendar rules
	"""
	def __build__(self):
		self.add(UsersRules("users"))
		self.add(OtherRules())

class UsersRules(RulesPattern):
	"""
	Node calendar/users
	"""
	def __build__(self):
		self.add(UserRules("$userId"))

class UserRules(RulesPattern):
	"""
	Node calendar/$userId
	"""
	def __build__(self):
		self.add(SettingsRules("settings", self.variables))
		self.add(AssosEventsRules("assos", self.variables))
		self.add(NotAssosEventsRules("notAssos", self.variables))
		self.add(PersoEventsRules("perso", self.variables))
		self.add(OtherRules())


# settings node
class SettingsRules(RulesPattern):
	"""
	Node calendar/$userId/settings
	"""
	def __build__(self):
		self.set_read(validate_uid(self.check_variable("$userId")))
		self.set_write(validate_uid(self.check_variable("$userId")))
		self.add(ResourcesRules("resources"))
		self.add(BooleanRules("icsDownload"))
		self.add(BooleanRules("assosEventsByDefault"))
		self.add(StringRules("keyHash", 64))
		self.add(StringRules("salt", 64))
		self.add(OtherRules())

class ResourcesRules(RulesPattern):
	"""
	Node calendar/$userId/settings/resources
	"""
	def __build__(self):
		self.set_validate("newData.val().matches(/^[0-9]*(,[0-9]+)*$/)")
		self.set_inline(True)


# assos events node
class AssosEventsRules(RulesPattern):
	"""
	Node calendar/$userId/assos
	"""
	def __build__(self):
		self.set_read(validate_uid(self.check_variable("$userId")))
		self.set_write(validate_uid(self.check_variable("$userId")))
		self.add(AssosEventRules("$eventId", self.variables))

class NotAssosEventsRules(RulesPattern):
	"""
	Node calendar/$userId/notAssos
	"""
	def __build__(self):
		self.set_read(validate_uid(self.check_variable("$userId")))
		self.set_write(validate_uid(self.check_variable("$userId")))
		self.add(AssosEventRules("$eventId", self.variables))

class AssosEventRules(RulesPattern):
	"""
	Node calendar/$userId/assos/$eventId
	Node calendar/$userId/notAssos/$eventId
	"""
	def __build__(self):
		self.add(IdRules("eventId", self.check_variable(self.label)))
		self.add(OtherRules())


# Perso events node
class PersoEventsRules(RulesPattern):
	"""
	Node calendar/perso
	"""
	def __build__(self):
		self.set_read(validate_uid(self.check_variable("$userId")))
		self.add(PersoEventRules("$eventId", self.variables))

class PersoEventRules(RulesPattern):
	"""
	Node calendar/perso/$eventId
	"""
	def __build__(self):
		self.set_write(validate_uid(self.check_variable("$userId")))
		self.add(StringRules("id", 30))
		self.add(StringRules("title", 80))
		self.add(NumberRules("start"))
		self.add(NumberRules("end"))
		self.add(BooleanRules("cipher"))
		self.add(BornedNumberRules("occurences", 1, 42))
		self.add(StringRules("location", 300))
		self.add(StringRules("type", 30))
		self.add(OtherRules())
