"""
Entry point for the ensicerclapp rules generation
"""

from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules
from users_rules import UsersRules
from admin_rules import AdminRules
from actus_rules import ActusRules
from cafet_rules import CafetRules
from nsigma_rules import NsigmaRules
from jobads_rules import JobAdsRules
from calendar_rules import CalendarRules
from events_rules import EventsRules
from vote_rules import VoteRules
from list_rules import ListRules

class MainRules(RulesPattern):
	"""
	The top node
	"""

	def __build__(self):
		self.add(UsersRules("users"))
		self.add(AdminRules("admin"))
		self.add(ActusRules("actus"))
		self.add(CafetRules("cafet"))
		self.add(NsigmaRules("nsigma"))
		self.add(JobAdsRules("jobads"))
		self.add(CalendarRules("calendar"))
		self.add(EventsRules("events"))
		self.add(VoteRules("vote"))
		self.add(ListRules("list"))

		self.add(OtherRules())

if __name__ == "__main__":
	RULES = MainRules("rules")

	# Generate rules
	with open("database/database_rules.json", "w") as file:
		file.write(RULES.to_json())

	# Generate UML diagrams
	with open("database/database_archi.puml", "w") as file:
		file.write(RULES.to_plantuml())

	for rule in RULES.rules_list:
		if rule.label == "$other":
			continue
		with open("database/modules_archi/archi_" + rule.label + ".puml", "w") as file:
			file.write(rule.to_plantuml())
