"""
A module dedicated to cafet data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, NumberRules
from frtdb_rules_generator.common import BornedNumberRules, BooleanRules, IdRules, do_and, do_or
from common import compute_email_id, is_one_admin, validate_email_id, is_member

def get_cafetresp_path():
	"""
	Returns the path in the database for cafet resp data
	"""
	return "'cafet/cafetResps/resps/'+" + compute_email_id()

def is_cafetresp():
	"""
	Returns a condition that is true if the authentified user is cafet resp
	"""
	cafetresp = "root.child(" + get_cafetresp_path() + ").exists()"
	return cafetresp + " && " + is_member()


class CafetRules(RulesPattern):
	"""
	Main node for cafet rules
	"""
	def __build__(self):
		self.set_read(is_one_admin("cafet"))
		self.set_write(is_one_admin("cafet"))

		self.add(CafetRespsRules("cafetResps"))
		self.add(ArchivesRules("archives"))
		self.add(ActiveUsersRules("users"))
		self.add(HistoryRules("history"))
		self.add(PublicDataRules("public"))
		self.add(OtherRules())


# common
class UserRules(RulesPattern):
	"""
	Node cafet/trezo/accounts/$emailId
	Node cafet/archives/users/$emailId
	Node cafet/users/$emailId
	"""
	def __build__(self):
		self.add(IdRules("emailId", self.check_variable(self.label)))
		self.add(BooleanRules("activated"))
		self.add(NumberRules("credit"))
		self.add(CreationDateRules("creationDate"))
		self.add(NumberRules("lastTransactionDate"))
		self.user_profile = self.add(ProfileRules("profile"))
		self.add(OtherRules())

class CreationDateRules(RulesPattern):
	"""
	Node cafet/trezo/accounts/$emailId/creationDate
	Node cafet/archives/users/$emailId/creationDate
	Node cafet/users/$emailId/creationDate
	"""
	def __build__(self):
		# compares to old data to avoid creating a new user with the same email_id as an old one
		self.set_validate(do_and([
				NumberRules.validate_number(),
				"!data.exists() || newData.val() === data.val()"
		]))
		self.set_inline(True)

class ProfileRules(RulesPattern):
	"""
	Node cafet/trezo/accounts/$emailId/profile
	Node cafet/archives/users/$emailId/profile
	Node cafet/users/$emailId/profile
	"""
	def __build__(self):
		child_label_1 = "firstName"
		child_label_2 = "lastName"
		self.set_validate("newData.hasChildren(['" + child_label_1 + "', '" + child_label_2 + "'])")
		self.add(StringRules(child_label_1, 30))
		self.add(StringRules(child_label_2, 30))
		self.add(StringRules("email", 50))
		self.add(BooleanRules("exte"))
		self.add(OtherRules())

	def should_be_null(self):
		"""
		This function should be called to require the profile to be null,
		for example when a profile is not needed
		"""
		self.set_validate("!newData.exists()")
		self.rules_list = []
		self.set_inline(True)


# cafet resps node
class CafetRespsRules(RulesPattern):
	"""
	Node cafet/cafetResps
	"""
	def __build__(self):
		self.set_read(is_cafetresp())
		self.add(CafetRespsListRules("resps"))
		self.add(DayTransactionsRules("dayTransactions"))
		self.add(OtherRules())

class CafetRespsListRules(RulesPattern):
	"""
	Node cafet/cafetResps/reps
	"""
	def __build__(self):
		self.add(CafetRespRules("$emailId"))

class CafetRespRules(RulesPattern):
	"""
	Node cafet/cafetResps/reps/$emailId
	"""
	def __build__(self):
		self.add(IdRules("emailId", self.check_variable(self.label)))
		self.add(OtherRules())

class DayTransactionsRules(RulesPattern):
	"""
	Node cafet/cafetResps/dayTransactions
	"""
	def __build__(self):
		self.set_write(is_cafetresp())
		self.add(DayUserRules("$emailId"))

class DayUserRules(RulesPattern):
	"""
	Node cafet/cafetResps/dayTransactions/$emailId
	"""
	def __build__(self):
		self.add(DayTransactionRules("$transaction"))

class DayTransactionRules(RulesPattern):
	"""
	Node cafet/cafetResps/dayTransactions/$emailId/$transaction
	"""
	def __build__(self):
		self.add(BornedNumberRules("value", -1000, 1000))
		self.add(NumberRules("date"))
		self.add(CafetRespRefRules("resp"))
		self.add(OtherRules())

class CafetRespRefRules(RulesPattern):
	"""
	Node cafet/cafetResps/dayTransactions/$emailId/$transaction/resp
	"""
	def __build__(self):
		self.set_validate("root.child('cafet/cafetResps/resps/'+newData.val()).exists()")
		self.set_inline(True)


# archives node
class ArchivesRules(RulesPattern):
	"""
	Node cafet/archives
	"""
	def __build__(self):
		self.add(ArchivedUsersRules("users"))
		self.add(OtherRules())

class ArchivedUsersRules(RulesPattern):
	"""
	Node cafet/archives/users
	"""
	def __build__(self):
		self.add(ArchivedUserRules("$emailId"))

class ArchivedUserRules(UserRules):
	"""
	Node cafet/archives/users/$emailId
	"""
	def __build__(self):
		super().__build__()
		still_user = "data.exists() && (newData.exists() && newData.val() !== null)"

		# when creating a user/changing id, make sure he doesn't already exists
		no_history = "!root.child('cafet/history/'+" + self.check_variable("$emailId") + ").exists()"

		# when archiving a user, writen user should be the same as the one that was active
		def get_former_active_user(data):
			old_active_user_path = "cafet/users/'+" + self.check_variable("$emailId") + "+'"
			return "root.child('" + old_active_user_path + "/" + data + "').val()"

		def get_newly_archived_user(data):
			return "newData.child('" + data + "').val()"

		same_user = do_and([
				get_former_active_user("credit") + " === " + get_newly_archived_user("credit"),
				get_former_active_user("creationDate") + " === " + get_newly_archived_user("creationDate")
		])

		new_user = do_and(["!data.exists()", do_or([no_history, same_user])])

		self.set_validate(do_or([still_user, new_user]))


# users node
class ActiveUsersRules(RulesPattern):
	"""
	Node cafet/users
	"""
	def __build__(self):
		self.set_read(is_cafetresp())
		self.add(ActiveUserRules("$emailId"))

class ActiveUserRules(UserRules):
	"""
	Node cafet/users/$emailId
	"""
	def __build__(self):
		super().__build__()
		modif_user = "data.exists() && (newData.exists() && newData.val() !== null)"

		# when creating a user/changing id, make sure he doesn't already exists
		no_history = "!root.child('cafet/history/'+" + self.check_variable("$emailId") + ").exists()"

		# when restoring a user, writen user should be the same as the one that was archived
		def get_former_archived_user(data):
			old_archived_user_path = "cafet/archives/users/'+" + self.check_variable("$emailId") + "+'"
			return "root.child('" + old_archived_user_path + "/" + data + "').val()"

		def get_newly_active_user(data):
			return "newData.child('" + data + "').val()"

		same_user = do_and([
				get_former_archived_user("credit") + " === " + get_newly_active_user("credit"),
				get_former_archived_user("creationDate") + " === " + get_newly_active_user("creationDate")
		])

		new_user = do_and(["!data.exists()", do_or([no_history, same_user])])

		# check for day transactions before deleting/changing id
		day_transactions_path = "'cafet/cafetResps/dayTransactions/'+" + self.check_variable("$emailId")
		delete_user = do_and([
				"data.exists()",
				"!newData.exists() || newData.val() === null",
				"!root.child(" + day_transactions_path + ").exists()"
		])

		self.set_validate(do_or([modif_user, new_user, delete_user]))
		self.set_read(validate_email_id(self.check_variable("$emailId")))


# history node
class HistoryRules(RulesPattern):
	"""
	Node cafet/history
	"""
	def __build__(self):
		self.add(UserHistoryRules("$emailId"))

class UserHistoryRules(RulesPattern):
	"""
	Node cafet/history/$emailId
	"""
	def __build__(self):
		self.set_read(validate_email_id(self.check_variable(self.label)))
		self.add(TransactionRules("$transaction"))

class TransactionRules(RulesPattern):
	"""
	Node cafet/history/$emailId/$transaction
	"""
	def __build__(self):
		self.add(BornedNumberRules("value", -1000, 1000))
		self.add(NumberRules("newCredit"))
		self.add(NumberRules("oldCredit"))
		self.add(NumberRules("date"))
		self.add(OtherRules())


# public data node
class PublicDataRules(RulesPattern):
	"""
	Node cafet/public
	"""
	def __build__(self):
		self.set_read(is_member())
		self.add(IngredientsRules("ingredients"))
		self.add(InfoRules("info"))
		self.add(OtherRules())

# Ingredients
# Note ingredients should be referenced with ids idenpent from names
class IngredientsRules(RulesPattern):
	"""
	Node cafet/public/ingredients
	"""
	def __build__(self):
		self.add(GroupsRules("groups"))
		self.add(IndividualRules("individual"))
		self.add(RecipesRules("recipes"))
		self.add(OtherRules())

class GroupsRules(RulesPattern):
	"""
	Node cafet/public/ingredients/$group
	"""
	def __build__(self):
		self.add(StringRules("$group", 30))

class IndividualRules(RulesPattern):
	"""
	Node cafet/public/ingredients/individual
	"""
	def __build__(self):
		self.add(IngredientRules("$ingredient"))

class IngredientRules(RulesPattern):
	"""
	Node cafet/public/ingredients/individual/$ingredient
	"""
	def __build__(self):
		self.add(StringRules("name", 30))
		self.add(StringRules("group", 30))
		self.add(StringRules("alias", 30))
		self.add(StringRules("image", 500))
		self.add(OtherRules())

class RecipesRules(RulesPattern):
	"""
	Node cafet/public/ingredients/$recipes
	"""
	def __build__(self):
		self.label = "recipes"
		self.add(RecipeRules("$recipe"))

class RecipeRules(RulesPattern):
	"""
	Node cafet/public/ingredients/$recipes/$recipe
	"""
	def __build__(self):
		self.add(StringRules("$ingredients", 30))


class InfoRules(RulesPattern):
	"""
	Node cafet/public/info
	"""
	def __build__(self):
		self.add(BooleanRules("service-opened"))
		self.add(BooleanRules("service-canceled"))
		self.add(StringRules("cancel-message", 200))
		self.add(NumberRules("day"))
		self.add(OtherRules())
