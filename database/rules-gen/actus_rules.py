"""
A module dedicated to actus data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, NumberRules, StringRules, IdRules
from frtdb_rules_generator.common import do_and, do_or
from common import compute_email_id, is_member, is_one_admin

def get_journalist_path():
	"""
	Returns the path in the database for journalist data
	"""
	return "'actus/journalists/users/'+" + compute_email_id()

def is_journalist():
	"""
	Returns a condition that is true if the authentified user is journalist
	"""
	journalist = "root.child(" + get_journalist_path() + ").exists()"
	return journalist + " && " + is_member()

def journalists_group_exists(group_id):
	"""
	Returns a condition that is true if the given group_id (newData.val())
	matches a group_id of the authentified user
	"""
	return do_or([
			do_and([
					"root.child(" + get_journalist_path() + "+'/groupId" + str(i) + "').exists()",
					"root.child(" + get_journalist_path() + "+'/groupId" + str(i) + "').val() !== null",
					"root.child(" + get_journalist_path() + "+'/groupId" + str(i) + "').val() !== ''",
					"root.child(" + get_journalist_path() + "+'/groupId" + str(i) + "').val() === " + group_id
			]) for i in range(1, 3)
	])

def journalists_group_the_same(snap):
	"""
	Returns a condition that is true if on of the comResp group_id matches
	one of the actu group_id
	"""
	return do_or([
			journalists_group_exists(snap + ".child('groupId" + str(i) + "').val()") for i in range(1, 2)
	])

def actus_group_exists(group_id):
	"""
	Returns a condition that is true if the given group_id is defined
	"""
	return "root.child('actus/journalists/groups/'+" + group_id + ").exists()"


class ActusRules(RulesPattern):
	"""
	Main node for actus
	"""
	def __build__(self):
		self.add(JournalistsRules("journalists"))
		self.add(ArticlesRules("actus"))
		self.add(OtherRules())


# journalists node
class JournalistsRules(RulesPattern):
	"""
	Node actus/journalists
	"""
	def __build__(self):
		self.set_read(do_or([is_journalist(), is_one_admin("actus")]))
		self.set_write(is_one_admin("actus"))

		self.add(GroupsRules("groups"))
		self.add(UsersRules("users"))
		self.add(OtherRules())

class GroupsRules(RulesPattern):
	"""
	Node actus/journalists/groups
	"""
	def __build__(self):
		self.set_read(is_member())
		self.add(GroupRules("$groupId"))

class GroupRules(RulesPattern):
	"""
	Node actus/journalists/groups/$groupId
	"""
	def __build__(self):
		self.add(StringRules("groupId", 30))
		self.add(StringRules("displayName", 30))
		self.add(OtherRules())

class UsersRules(RulesPattern):
	"""
	Node actus/journalists/users
	"""
	def __build__(self):
		self.add(UserRules("$emailId"))

class UserRules(RulesPattern):
	"""
	Node actus/journalists/users/$emailId
	"""
	def __build__(self):
		self.add(IdRules("emailId", self.check_variable(self.label)))
		self.add(GroupIdRules("groupId1"))
		self.add(GroupIdRules("groupId2"))
		self.add(OtherRules())

class GroupIdRules(RulesPattern):
	"""
	Node actus/journalists/users/$emailId/groupId1
	Node actus/journalists/users/$emailId/groupId2
	"""
	def __build__(self):
		self.set_validate(do_and([
				StringRules.validate_string(30),
				actus_group_exists("newData.val()")
		]))
		self.set_inline(True)

# actus node
class ArticlesRules(RulesPattern):
	"""
	Node actus/actus
	"""
	def __build__(self):
		self.set_read(is_member())
		self.index_on = ["date"]
		self.add(ArticleRules("$actuId"))

class ArticleRules(RulesPattern):
	"""
	Node actus/actus/$actuId
	"""
	def __build__(self):
		self.set_write(do_or([
				is_one_admin("actus"),
				do_and([
						is_journalist(),
						do_or([
								journalists_group_the_same("newData"),
								do_and([
										"!newData.exists()",
										journalists_group_the_same("data")
								])
						])
				])
		]))

		self.add(IdRules("id", self.check_variable(self.label)))
		self.add(StringRules("title", 30))
		self.add(StringRules("description", 2000))
		self.add(StringRules("image", 500))
		self.add(StringRules("pdfLink", 500))
		self.add(NumberRules("date"))
		self.add(StringRules("author", 50))
		self.add(ArticleGroupIdRules("groupId1"))
		self.add(OtherRules())

class ArticleGroupIdRules(RulesPattern):
	"""
	Node actus/actus/$actuId/groupId1
	"""
	def __build__(self):
		self.set_validate(do_and([
				StringRules.validate_string(30),
				actus_group_exists("newData.val()")
		]))
		self.set_inline(True)
