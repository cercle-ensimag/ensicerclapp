"""
A module dedicated to job ads data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, BooleanRules, StringRules
from frtdb_rules_generator.common import BornedNumberRules, NumberRules, IdRules
from common import is_member, is_one_admin

class JobAdsRules(RulesPattern):
	"""
	Main node for job ads rules
	"""
	def __build__(self):
		self.add(JobAdsListRules("jobads"))
		self.add(OtherRules())

# jobads list node
class JobAdsListRules(RulesPattern):
	"""
	Node jobads/jobads
	"""
	def __build__(self):
		self.set_read(is_member())
		self.set_write(is_one_admin("jobads"))
		self.index_on = ["start"]
		self.add(JobAdRules("$jobadId"))

# jobad node
class JobAdRules(RulesPattern):
	"""
	Node jobads/jobads/$jobadId
	"""
	def __build__(self):
		self.set_write(is_one_admin("jobads"))

		self.add(IdRules("id", self.check_variable(self.label)))
		self.add(StringRules("title", 50))
		self.add(StringRules("description", 5000))
		self.add(BornedNumberRules("type", 0, 2))
		self.add(NumberRules("start"))
		self.add(StringRules("length", 20))
		self.add(StringRules("technologies", 100))
		self.add(StringRules("contact", 200))
		self.add(StringRules("author", 30))
		self.add(BooleanRules("done"))
		self.add(OtherRules())
