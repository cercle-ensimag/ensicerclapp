# Firebase database

## Generating rules

### Setup

Generating the rules requires `frtdb_rules_generator` python package.

It is currently tracked in a sub module in the project.

First make sure the submodule is syncronized with:
```
git submodule update --init
```

Then install it locally with:
```
cd rules-gen/frtdb-rules-generator/
python3 -m pip install -e .
```

### Execution

Run `./compile-rules.sh` to generate the rules.


## Database diagrams

Along with the rules are generated [PlantUML](https://plantuml.com) diagrams of the database.

The file `database_archi.puml` contains the hole database architecture.

The folder `modules_archi/` contains a diagrams per sub system of the project.

To visualize the diagrams you can paste them to [PlantUML Server](https://www.plantuml.com).
